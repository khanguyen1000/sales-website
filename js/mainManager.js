//Tạo 1 product List

let productList = [];
let cartList = [];

//function 1 :Lấy sản phẩm từ database
const fetchProduct = function () {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",

    method: "GET",
  })
    .then(function (res) {
      mapData(res.data);
      // console.log(res.data);
      renderProducts();
    })
    .catch(function (err) {
      console.log(err);
    });
};

//function 2: hiển thị danh sách sản phâm ra màn hình
const renderProducts = function () {
  let htmlContent = "";
  for (let i in productList) {
    htmlContent += productList[i].renderManager(+i + 1);
  }
  document.getElementById("renderProducts").innerHTML = htmlContent;
};

//function 3 : maps danh sách
const mapData = function (data) {
  productList = [];
  for (let item of data) {
    const {
      description,
      id,
      image,
      inventory,
      name,
      price,
      rating,
      type,
    } = item;
    item = new Products(
      description,
      id,
      image,
      inventory,
      name,
      price,
      rating,
      type
    );
    productList.push(item);
  }
};

//function 4: sắp xếp mảng từ A-Z
const sortAZ = () => {
  //Cach 1 nhung k xai duoc ki tu ngaoi ma ASCII
  // productList.sort(function (a, b) {
  //   var nameA = a.name.toUpperCase(); // bỏ qua hoa thường
  //   var nameB = b.name.toUpperCase(); // bỏ qua hoa thường
  //   if (nameA < nameB) {
  //     return -1;
  //   }
  //   if (nameA > nameB) {
  //     return 1;
  //   }

  //   // name trùng nhau
  //   return 0;
  // });
  // console.log(productList);

  //hàm sorf => localeCompare sắp xếp mảng kí tự đặc biệt ngoài mã ASCII
  productList.sort(function (a, b) {
    return a.name.localeCompare(b.name);
  });
  renderProducts();
};

//function 5: sắp xếp mảng từ Z-A
const sortZA = () => {
  productList.sort(function (a, b) {
    return b.name.localeCompare(a.name);
  });
  renderProducts();
};

//function 6:  Filter Iphone - Samsung - Others
const selectProducts = () => {
  const listIphone = [];
  const listSamsung = [];
  const listOthers = [];
  for (let item of productList) {
    let type = item.type.toLowerCase();
    if (type === "iphone") {
      listIphone.push(item);
    } else if (type === "samsung") {
      listSamsung.push(item);
    } else {
      listOthers.push(item);
    }
  }

  console.log(listIphone);
  let htmlContent = "";
  let choose = document.getElementById("mySelect").value;
  //List Iphone
  if (choose === "Iphone") {
    for (var i in listIphone) {
      htmlContent += listIphone[i].renderManager(+i + 1);
    }
  }
  //List Samsung
  else if (choose === "Samsung") {
    for (var i in listSamsung) {
      htmlContent += listSamsung[i].renderManager(+i + 1);
    }
  }
  //List Others
  else if (choose === "Others") {
    for (var i in listOthers) {
      htmlContent += listOthers[i].renderManager(+i + 1);
    }
  } else {
    for (let i in productList) {
      htmlContent += productList[i].renderManager(+i + 1);
    }
  }

  document.getElementById("renderProducts").innerHTML = htmlContent;
};

//function findIndex
const findIndexCart = (id) => {
  let index = -1;
  index = cartList.findIndex(function (itemCart) {
    return parseInt(itemCart.item.id) === parseInt(id);
  });
  return index;
};
const getProdById = (id) => {
  var prod = [];
  for (let i of productList) {
    if (+i.item.id === id) {
      prod = item;
    }
  }

  // prod = productList.item.find(function (itemc) {
  //   //find tìm toàn bộ item
  //   return parseInt(itemc.item.id) === parseInt(id);
  // });

  return prod;
};

//Phần 2 : quản lý -----------------------------------------
//funciton 1 : Thêm sản phẩm
const addProduct = () => {
  //1. lấy dữ liệu người dùng nhập
  const id = document.getElementById("txtId").value;
  const description = document.getElementById("txtDes").value;
  const name = document.getElementById("txtName").value;
  const image = document.getElementById("txtImage").value;
  const price = document.getElementById("txtPrice").value;
  const inventory = document.getElementById("txtInven").value;
  const rating = document.getElementById("txtRating").value;
  const type = document.getElementById("txtType").value;
  //2. tạo một đối products từ dữ liệu ngta nhập
  if (id === "") {
    return false;
  }
  const newProducts = new Products(
    description,
    id,
    image,
    inventory,
    name,
    price,
    rating,
    type
  );
  //3.call api tới back -end nhờ thêm dùm nhân viên mới vào DB
  axios({
    method: "POST",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    data: newProducts,
  })
    .then(function (res) {
      //call api lấy danh sách mới
      fetchProduct();
      swal("Đã Thêm thành công  !", {
        icon: "success",
      });
    })
    .catch(function (err) {
      console.log(err);
    });
};

//funciton 2 : Edit sản phẩm (xem)
const editProducts = (ide) => {
  const id = document.getElementById("txtId").value;
  const description = document.getElementById("txtDes").value;
  const name = document.getElementById("txtName").value;
  const image = document.getElementById("txtImage").value;
  const price = document.getElementById("txtPrice").value;
  const inventory = document.getElementById("txtInven").value;
  const rating = document.getElementById("txtRating").value;
  const type = document.getElementById("txtType").value;
  const editPro = new Products(
    description,
    id,
    image,
    inventory,
    name,
    price,
    rating,
    type
  );
  swal({
    title: "Bạn chắc chứ?",
    text: "nếu OK dữ liệu về sản phẩm này sẽ thay đổi!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
    .then((willUpdate) => {
      if (willUpdate) {
        axios({
          method: 'PUT',
          url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${ide}`,
          data: editPro
        }).then((res) => {
          console.log(res);
          fetchProduct();
        }).catch((err) => {
          console.log(err);
        });
        swal("Đã cập nhật thành công  !", {
          icon: "success",
        });
      } else {
        swal("Dữ liệu vẫn như cũ!");
      }
    });



};
//function 3: Xóa sản phẩm
const deleteProducts = (id) => {
  swal({
    title: "Bạn chắc chứ?",
    text: "nếu xóa rồi dữ liệu về sản phẩm này không thể khôi phục!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        axios({
          method: "DELETE",
          url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
        })
          .then((res) => {
            fetchProduct();
          })
          .catch((err) => {
            console.log(err);
          });
        swal("Đã xóa thành công  !", {
          icon: "success",
        });
      } else {
        swal("Dữ liệu vẫn như cũ!");
      }
    });

};
// function : tạo render modal
const renderModal = (title, action, btn, ide = "") => {
  axios({
    method: "GET",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + ide,
  })
    .then((res) => {
      let item = res.data;
      const { id, name, image, description,
        price, inventory, rating, type } = item;
      let contentHtml = "";
      if (ide !== "") {
        contentHtml = `<div class="modal-header">
          <h5 class="modal-title">${title}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div>
            <form action="">
              <div class="d-flex">
                <div class="col-md-6 addProducts">
                  <div class="form-group">
                    <label for="txtId" class="font-weight-bold">ID:</label>
                    <input type="text" class="form-control" id="txtId" required value="${id}">
                  </div>
                  <div class="form-group">
                    <label for="txtDes" class="font-weight-bold">Description :</label>
                    <input type="text" class="form-control" id="txtDes" required value="${description}">
                  </div>
                  <div class="form-group">
                    <label for="txtName" class="font-weight-bold">Name :</label>
                    <input type="text" class="form-control" id="txtName" required value="${name}">
                  </div>
                  <div class="form-group">
                    <label for="txtImage" class="font-weight-bold">Image :</label>
                    <input type="text" class="form-control" id="txtImage" required value="${image}">
                  </div>
                </div>
                <div class="col-md-6 addProducts">
                  <div class="form-group">
                    <label for="txtPrice" class="font-weight-bold">Price :</label>
                    <input type="text" class="form-control" id="txtPrice" required value="${price}">
                  </div>
                  <div class="form-group">
                    <label for="txtInven" class="font-weight-bold">Inventory :</label>
                    <input type="text" class="form-control" id="txtInven" required value="${inventory}">
                  </div>
                  <div class="form-group">
                    <label for="txtRating" class="font-weight-bold">Rating :</label>
                    <input type="text" class="form-control" id="txtRating" required value="${rating}">
                  </div>
                  <div class="form-group">
                    <label for="txtType" class="font-weight-bold">Type :</label>
                    <input type="text" class="form-control" id="txtType" required value="${type}">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-warning" data-dismiss="modal"
            onclick="${action}">${btn}</button>
        </div>`;
      } else {
        contentHtml = `<div class="modal-header">
        <h5 class="modal-title">${title}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
          <form action="">
            <div class="d-flex">
              <div class="col-md-6 addProducts">
                <div class="form-group">
                  <label for="txtId" class="font-weight-bold">ID:</label>
                  <input type="text" class="form-control" id="txtId" required value="">
                </div>
                <div class="form-group">
                  <label for="txtDes" class="font-weight-bold">Description :</label>
                  <input type="text" class="form-control" id="txtDes" required value="">
                </div>
                <div class="form-group">
                  <label for="txtName" class="font-weight-bold">Name :</label>
                  <input type="text" class="form-control" id="txtName" required value="">
                </div>
                <div class="form-group">
                  <label for="txtImage" class="font-weight-bold">Image :</label>
                  <input type="text" class="form-control" id="txtImage" required value="">
                </div>
              </div>
              <div class="col-md-6 addProducts">
                <div class="form-group">
                  <label for="txtPrice" class="font-weight-bold">Price :</label>
                  <input type="text" class="form-control" id="txtPrice" required value="">
                </div>
                <div class="form-group">
                  <label for="txtInven" class="font-weight-bold">Inventory :</label>
                  <input type="text" class="form-control" id="txtInven" required value="">
                </div>
                <div class="form-group">
                  <label for="txtRating" class="font-weight-bold">Rating :</label>
                  <input type="text" class="form-control" id="txtRating" required value="">
                </div>
                <div class="form-group">
                  <label for="txtType" class="font-weight-bold">Type :</label>
                  <input type="text" class="form-control" id="txtType" required value="">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal"
          onclick="${action}">${btn}</button>
      </div>`;
      }
      document.getElementById("modalContent").innerHTML = contentHtml;
    })
    .catch((err) => {
      console.log(err);
    });

}
fetchProduct();
