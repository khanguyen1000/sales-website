class Products {
  constructor(description, id, image, inventory, name, price, rating, type) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.description = description;
    this.price = price;
    this.inventory = inventory;
    this.rating = rating;
    this.type = type;
    this.item = {
      id: this.id, name: this.name, image: this.image, description: this.description,
      price: this.price,
      inventory: this.inventory,
      rating: this.rating,
      type: this.type,
    }
  }

  render() {

    return `
    <div class="col-4 mb-3">
        <div class="card p-2">
          <img src=${this.image} class="img-fluid imgCard" alt="${this.image}"/>
          <div class="content">
            <p class="nameTag">${this.name}</p>
            <p class="discription">${this.description}</p>
            <p class="type"> ${this.type} </p>
            <div class="d-flex align-items-center">
            <p class="price">${new Intl.NumberFormat('vi-VI', {
      style: 'currency',
      currency: 'VND',
    }).format(this.price)}</p>
            <button class="btn btn-success addCart" onclick="addToCart(${this.id}),renderCart()"><img src="./img/cart.svg" class="img-fluid"></button>
        </div>
            </div>
          </div>
          
      </div>
    `;
  }

  renderManager(i) {
    return `
    <tr>
              <th scope="row">${i}</th>
              <td>${this.id}</td>
              <td><img src=${this.image} class="img-fluid" style="width: 100px;height: 100px;"></td>
              <td>${this.name}</td>
              <td>${this.inventory}</td>
              <td>${new Intl.NumberFormat('vi-VI', {
      style: 'currency',
      currency: 'VND',
    }).format(this.price)}</td>
              <td>
                <div class="d-flex justify-content-start">
                  <button class="btn btnEdit btn-info rounded-circle mr-2" data-toggle="modal" data-target=".bd-example-modal-lg" 
                  onclick="renderModal('Sửa Thông Tin','editProducts(${this.id})','Edit','${this.id}')"
                  ><img src="./img/edit.svg"></button>
                  <button class="btn btn-danger rounded-circle" onclick="deleteProducts(${this.id})"><img src="./img/delete.svg"></button>
                </div>
              </td>
            </tr> 
    `;
  }
}
