//Tạo 1 product List

const productList = [];
let cartList = [];

//function 1 :Lấy sản phẩm từ database
const fetchProduct = function () {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    // url: "../Test.json",
    method: "GET",
  })
    .then(function (res) {
      mapData(res.data);
      // console.log(res.data);
      renderProducts();
    })
    .catch(function (err) {
      console.log(err);
    });
};

//function 2: hiển thị danh sách sản phâm ra màn hình
const renderProducts = function () {
  let htmlContent = "";
  for (let i in productList) {
    htmlContent += productList[i].render();
  }
  document.getElementById("renderProducts").innerHTML = htmlContent;
};

//function 3 : maps danh sách
const mapData = function (data) {
  let newProducts;
  for (let item of data) {
    const {
      description,
      id,
      image,
      inventory,
      name,
      price,
      rating,
      type,
    } = item;
    newProducts = new Products(
      description,
      id,
      image,
      inventory,
      name,
      price,
      rating,
      type
    );
    productList.push(newProducts);
  }
};

//function 4: sắp xếp mảng từ A-Z
const sortAZ = () => {
  //Cach 1 nhung k xai duoc ki tu ngaoi ma ASCII
  // productList.sort(function (a, b) {
  //   var nameA = a.name.toUpperCase(); // bỏ qua hoa thường
  //   var nameB = b.name.toUpperCase(); // bỏ qua hoa thường
  //   if (nameA < nameB) {
  //     return -1;
  //   }
  //   if (nameA > nameB) {
  //     return 1;
  //   }

  //   // name trùng nhau
  //   return 0;
  // });
  // console.log(productList);

  //hàm sorf => localeCompare sắp xếp mảng kí tự đặc biệt ngoài mã ASCII
  productList.sort(function (a, b) {
    return a.name.localeCompare(b.name);
  });
  renderProducts();
};

//function 5: sắp xếp mảng từ Z-A
const sortZA = () => {
  productList.sort(function (a, b) {
    return b.name.localeCompare(a.name);
  });
  renderProducts();
};

//function 6:  Filter Iphone - Samsung - Others
const selectProducts = () => {
  const listIphone = [];
  const listSamsung = [];
  const listOthers = [];
  for (let item of productList) {
    if (item.type === "Iphone" || item.type === "iphone") {
      listIphone.push(item);
    } else if (item.type === "Samsung" || item.type === "samsung") {
      listSamsung.push(item);
    } else {
      listOthers.push(item);
    }
  }

  console.log(listIphone);
  let htmlContent = "";
  let choose = document.getElementById("mySelect").value;
  //List Iphone
  if (choose === "Iphone") {
    for (var i in listIphone) {
      htmlContent += listIphone[i].render();
    }
  }
  //List Samsung
  else if (choose === "Samsung") {
    for (var i in listSamsung) {
      htmlContent += listSamsung[i].render();
    }
  }
  //List Others
  else if (choose === "Others") {
    for (var i in listOthers) {
      htmlContent += listOthers[i].render();
    }
  } else {
    for (let i in productList) {
      htmlContent += productList[i].render();
    }
  }

  document.getElementById("renderProducts").innerHTML = htmlContent;
};

//function 7:add to cart
const addToCart = (id) => {
  // console.log(id);
  var cartItem;
  // var cartItem = {
  //   product: { id: 1, price: 10, name: "samsung a10" },
  //   quantity: 1,
  // };
  for (let item of productList) {
    if (+item.id === id) {
      cartItem = {
        item,
        quantity: 1,
      };
      for (var itemcart of cartList) {
        if (itemcart.item.id === item.id) {
          return itemcart.quantity++;
          // break;
        }
        //  else {
        //   var newItem = {
        //     item,
        //     quantity: 1,
        //   };
        //   cartList.push(newItem);
        // }
      }
      cartList.push(cartItem);
    }
  }
  swal("Thành công!", "Vui lòng kiểm tra giỏ hàng!", "success");
  // console.log(cartList);
  document.getElementById("calcSum").innerHTML = "0.0 đ";
};

//function 8: In giỏ hàng ra màng hình

const renderCart = () => {
  let htmlContent = "";
  for (let i of cartList) {
    htmlContent += `<tr>
    <td class="text-center">
      <img
        style="width: 50px; height:50px"
        src=${i.item.image}
        class="img-fluid"
      />
    </td>
    <td style="font-size: 20px;text-align:center;">${i.item.name}</td>
    <td>${new Intl.NumberFormat('vi-VI', {
      style: 'currency',
      currency: 'VND',
    }).format(+i.item.price)}</td>
    <td>
    <div class="d-flex justify-content-center align-items-center">
      ${i.quantity}
      <div class="group-btn ml-2">
      <button class="btn btn-info border-left btnQuantity mr-2" onclick="upQuantity(${+i.item
        .id})">+</button>
        <button class="btn btn-info border-right btnQuantity" onclick="downQuantity(${+i
        .item.id})">-</button>
      </div>
    </div>
      
    </td>
    <td class="text-center">${new Intl.NumberFormat('vi-VI', {
          style: 'currency',
          currency: 'VND',
        }).format((+i.item.price) * (+i.quantity))}</td>
    <td>
      <button class="btn btn-info btnCartRemove"  onclick="removeProducts(${+i.item
        .id})">x</button>
    </td>
  </tr>
  </br></br>
  `;
  }
  document.getElementById("tbodyCart").innerHTML = htmlContent;
};

//function 9: Hàm tăng số lượng sản phẩm trong cart
const upQuantity = (id) => {
  for (let itemCart of cartList) {
    if (+itemCart.item.id === id) {
      ++itemCart.quantity;
    }
  }
  renderCart();
};
//function 10: Hàm giảm số lượng sản phẩm trong cart
const downQuantity = (id) => {
  for (let itemCart of cartList) {
    if (+itemCart.item.id === id) {
      if (itemCart.quantity > 0) {
        --itemCart.quantity;
      } else {
        swal("Please quantity is greater than 0");
      }
    }
  }

  renderCart();
};

//function 11: Hàm tổng tiền giỏ hàng
const calcSum = () => {
  var sum = 0;
  for (let itemCart of cartList) {
    sum += itemCart.item.price * itemCart.quantity;
  }
  let btnRemove = document.getElementsByClassName("btnCartRemove");
  for (let btn of btnRemove) {
    btn.style.display = 'none';
  }
  let btnQuantity = document.getElementsByClassName("btnQuantity");
  for (let btn of btnQuantity) {
    btn.style.display = 'none';
  }
  document.getElementById("calcSum").innerHTML = new Intl.NumberFormat('vi-VI', {
    style: 'currency',
    currency: 'VND',
  }).format(sum);
  if (cartList.length === 0) {
    swal({
      title: "Giỏ hàng trống!!",
      text: "Bạn không thể thanh toán! vui lòng chọn sản phẩm",
      icon: "warning",
    });
    renderCart();
  } else {
    swal(`Tổng số tiền là : ${new Intl.NumberFormat('vi-VI', {
      style: 'currency',
      currency: 'VND',
    }).format(sum)}`)
      .then(() => {
        swal(`Thank you !!`);
        setLocalStorage();
        cartList.splice(0, cartList.length);
        renderCart();
        document.getElementById("calcSum").innerHTML = "0 đ";
      });
  }

};

const setLocalStorage = () => {
  localStorage.setItem("ListCart", JSON.stringify(cartList));
};
const getLocalStorage = () => {
  if (localStorage.getItem("ListCart")) {
    cartList=JSON.parse(localStorage.getItem('ListCart'));
    renderCart();
  }
};
//function findIndex
const findIndexCart = (id) => {
  let index = -1;
  index = cartList.findIndex(function (itemCart) {
    return parseInt(itemCart.item.id) === parseInt(id);
  });
  return index;
};
//function 14: remove 1 sản phẩm trong cart
const removeProducts = (id) => {
  var index = findIndexCart(id);
  swal({
    title: "Bạn chắc chứ?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      if (index !== -1) {
        cartList.splice(index, 1);
      }
      swal("Xóa thành công!", {
        icon: "success",
      });
      renderCart();
      setLocalStorage();
    } else {
      swal("Thật là tốt!");
    }
  });
  
};
//function 15: Hàm đăng nhập
const checkManager = () => {
  let id = document.getElementById("txtUser").value;
  let pass = document.getElementById("txtPass").value;
  console.log(id, pass);
  if (id == "admin") {
    if (pass == "123") {
      return true;
    } else {
      swal("password : 123");
    }
  } else {
    swal("user : admin");
  }
  return false;
};

//Phần 2 : quản lý -----------------------------------------

//function 1:
const renderProductsManager = function () {
  let htmlContent = "";
  for (let i in productList) {
    htmlContent += productList[i].renderManager();
  }
  document.getElementById("renderProducts").innerHTML = htmlContent;
};
fetchProduct();
getLocalStorage();
